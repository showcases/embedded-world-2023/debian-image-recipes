# Embedded World Image Recipes

A collection of Debos recipes to build images for Embedded World demos.

## Prebuilt images
See the [latest CI/CD pipeline artifacts](https://gitlab.collabora.com/showcases/embedded-world-2023/debian-image-recipes/-/jobs/artifacts/embedded-world-2023/browse?job=build%20rockchip%20ml%20compression%20images)
to download prebuilt images for your target.

These images can be flashed to an SD card with bmaptool or can be flashed to an eMMC.

## Flashing to SD card using bmaptool

Copy the image to an SD card:

```
$ bmaptool copy image-rockchip-rock-pi-4-rk3399.img.gz /dev/<your-block-device-here>
```

Remember to remove any eMMC from the board.


## Flashing to eMMC using rockusb

Before powering the board, put the board into maskrom mode by either:
- booting the device with no eMMC connected
- shorting out `SW1500` pads

After applying power, make sure an eMMC is plugged into the board.
Plug a USB-A cable into the top USB3 port and to your PC.
Download the image:

```
$ cargo install rockusb --example rockusb
$ rockusb download-boot rk3399_loader_*.bin
$ rockusb write-bmap image-rockchip-rock-pi-4-rk3399.img.gz
```
